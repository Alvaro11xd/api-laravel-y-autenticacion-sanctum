<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function index()
    {
        /**
         * Seleccionar a todos los empleados
         * Seleccionar la columna name de la tabla departaments y darle el alias departament
         * unir la tabla employees con departaments por el id de departaments y departament_id de employees
         */
        $employees = Employee::select('employees.*', 'departaments.name as departament')
            ->join('departaments', 'departaments.id', '=', 'employees.departament_id')
            ->paginate(10);
        return response()->json($employees);
    }
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:100',
            'email' => 'required|email|max:80',
            'phone' => 'required|max:15',
            'departament_id' => 'required|numeric'
        ];
        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }
        $employee = new Employee($request->input());
        $employee->save();
        return response()->json([
            'status' => true,
            'message' => 'Empleado creado satisfactoriamente'
        ], 200);
    }
    public function show(Employee $employee)
    {
        return response()->json(['status' => true, 'data' => $employee]);
    }
    public function update(Request $request, Employee $employee)
    {
        $rules = [
            'name' => 'required|string|min:1|max:100',
            'email' => 'required|email|max:80',
            'phone' => 'required|max:15',
            'departament_id' => 'required|numeric'
        ];
        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }
        $employee->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Empleado actualizado satisfactoriamente'
        ], 200);
    }
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json([
            'status' => true,
            'message' => 'Empleado eliminado satisfactoriamente'
        ], 200);
    }

    public function EmployeeByDepartament(){
        $employees= Employee::select(DB::raw('count(employees.id) as count, departaments.name'))
        ->rightJoin('departaments', 'departaments.id', '=', 'employees.departament_id')
        ->groupBy('departaments.name')->get();
        return response()->json($employees);
    }

    public function all(){
        $employees = Employee::select('employees.*', 'departaments.name as departament')
            ->join('departaments', 'departaments.id', '=', 'employees.departament_id')
            ->get();
        return response()->json($employees);
    }
}
