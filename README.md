<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

# API CON LARAVEL Y AUTENTICACIÓN SANCTUM

Este es un proyecto que se basa en la creación de una API para elaborar un CRUD de empleados y departamentos, ambos relacionados y a parte se usó Sanctum para la autenticación

## COMANDOS USADOS

- composer create-project laravel/laravel company --prefer-dist -> para crear el proyecto Laravel.
- php artisan make:mode Departament -mcrf --api -> para crear el modelo, migración, factory y controlador con los métodos necesarios para desarrollar el crud de la entidad Departamentos.
- php artisan make:mode Employee -mcrf --api -> para crear el modelo, migración, factory y controlador con los métodos necesarios para desarrollar el crud de la entidad Empleados.
- php artisan make:controller AuthController -> para crear el controlador de Autenticación, necesario para crear, iniciar sesión y cerrar sesión.
