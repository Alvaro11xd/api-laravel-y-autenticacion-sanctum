<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DepartamentController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

// Creando las rutas para el usuario
Route::post('auth/register', [AuthController::class, 'create']);
Route::post('auth/login', [AuthController::class, 'login']);

// Protegiendo las rutas
Route::middleware(['auth:sanctum'])->group(function () {
    // Creando las rutas
    Route::resource('departaments', DepartamentController::class);
    Route::resource('employees', EmployeeController::class);
    Route::get('employeesall', [EmployeeController::class, 'all']);
    Route::get('employeebydepartament', [EmployeeController::class, 'EmployeeByDepartament']);
    Route::get('auth/logout', [AuthController::class, 'logout']);
});

